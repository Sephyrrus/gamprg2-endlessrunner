// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 *
 */
UCLASS()
class ENDLESSRUNNER_API ARunGameMode : public AGameModeBase {
	GENERATED_BODY()

private: 
	
protected:
	FVector SpawnPoint = FVector(0, 0, 0);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile Class Reference")
	TSubclassOf<class ATile>  TileReference;

	UFUNCTION()
	void OnTileExit(ATile* Tile);

	void AddFloorTile();
	void SetDestroyTile(ATile* tile);
public:
	ARunGameMode();
	virtual void BeginPlay() override;
};
