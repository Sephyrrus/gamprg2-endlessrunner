// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeath, ARunCharacter*, RunCharacter);

UCLASS()
class ENDLESSRUNNER_API ARunCharacter : public ACharacter {
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USpringArmComponent* SpringArm;

	bool isAlive = true;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	bool GetisAlive();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Coins;

	UFUNCTION(BlueprintCallable)
	void AddCoin(float value);

	UFUNCTION(BlueprintCallable)
	void Die();

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
	FOnDeath OnDeath;
};
