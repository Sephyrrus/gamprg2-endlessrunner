

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTileExited, ATile*, Tile);

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Obstacle Class Reference")
		TArray <TSubclassOf<class AObstacle>> ObstacleTypes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pikcup Class Reference")
		TArray <TSubclassOf<class APickup>> PickupTypes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Chances")
		float ObstacleSpawnChance = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Chances")
		float PickupSpawnChance = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ObstacleSpawnArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* PickupSpawnArea;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	void SpawnObstacle();
	void SpawnPickup();
	void CreateSpawnables(TArray <TSubclassOf<class AActor>> SpawnableTypes, UBoxComponent* SpawnPoint);
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void DestroyTile();
	FVector GetAttachPointLocation();

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnTileExited OnTileExited;
};
