// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacter.h"
#include "RunCharacterController.generated.h"

UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacterController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	ARunCharacter* Player;
	void MoveRight(float scale);
	void MoveForward(float scale);

public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
};
