


#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ATile::ATile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(RootComponent);
	AttachPoint->SetRelativeLocation(FVector(500, 0, 0));

	ObstacleSpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawnPoint"));
	ObstacleSpawnArea->SetupAttachment(RootComponent);

	PickupSpawnArea = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupSpawnArea"));
	PickupSpawnArea->SetupAttachment(RootComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(RootComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnOverlapBegin);

}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	if (rand() % 100 <= ObstacleSpawnChance)
		SpawnObstacle();
	if (rand() % 100 <= PickupSpawnChance)
		SpawnPickup();
}

void ATile::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* Character = Cast<ARunCharacter>(OtherActor)) {
		OnTileExited.Broadcast(this);
	}
}
// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
void ATile::SpawnObstacle() {
	FVector SpawnLocation;
	FActorSpawnParameters SpawnParameters;

	AObstacle* SpawnedObstacle;
	FAttachmentTransformRules rules(EAttachmentRule::KeepRelative, false);

	SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetRelativeLocation(), ObstacleSpawnArea->GetScaledBoxExtent());
	SpawnedObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleTypes[FMath::RandRange(0,ObstacleTypes.Num() - 1)],
																	  SpawnLocation, 
																	  FRotator(0),
																	  SpawnParameters);
	SpawnedObstacle->AttachToActor(this, rules);
}

void ATile::SpawnPickup() {
	FVector SpawnLocation;
	FActorSpawnParameters SpawnParameters;

	APickup* SpawnedPickup;
	FAttachmentTransformRules rules(EAttachmentRule::KeepRelative, false);

	SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(PickupSpawnArea->GetRelativeLocation(), PickupSpawnArea->GetScaledBoxExtent());
	SpawnedPickup = GetWorld()->SpawnActor<APickup>(PickupTypes[FMath::RandRange(0, PickupTypes.Num() - 1)],
																SpawnLocation + FVector(0,0, 100), 
																FRotator(90,0,0),
																SpawnParameters);
	SpawnedPickup->AttachToActor(this, rules);
}

void ATile::DestroyTile() {
	if (IsValidLowLevel()) {
		TArray<AActor*> Spawnable;
		GetAttachedActors(Spawnable);

		for (int x = 0; x < Spawnable.Num(); ++x) {
				Spawnable[x]->Destroy();
		}
		Destroy();
	}
}
FVector ATile::GetAttachPointLocation() {
	return AttachPoint->GetComponentLocation();
}
