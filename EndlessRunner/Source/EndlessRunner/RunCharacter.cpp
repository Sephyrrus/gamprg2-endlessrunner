

#include "RunCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "RunCharacterController.h"

ARunCharacter::ARunCharacter(){
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetMesh());
	SpringArm->TargetArmLength = 500.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay(){
	Super::BeginPlay();

}

// Called every frame
void ARunCharacter::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void ARunCharacter::AddCoin(float value){
	Coins += value;
}

bool ARunCharacter::GetisAlive() {
	return isAlive;
}

void ARunCharacter::Die() {
	if (isAlive == true) {
		isAlive = false;
		
		GetMesh()->SetVisibility(false);
		GetController()->DisableInput(Cast<ARunCharacterController>(GetController()));

		OnDeath.Broadcast(this);
	}
}