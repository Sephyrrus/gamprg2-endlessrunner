


#include "Obstacle.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "RunCharacter.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	SetRootComponent(Mesh);
	Mesh->OnComponentHit.AddDynamic(this, &AObstacle::OnCompHit);
}

void AObstacle::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
	if (ARunCharacter* Character = Cast<ARunCharacter>(OtherActor)) {
		OnTrigger(Character);
	}
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay(){
	Super::BeginPlay();
	
}
// Called every frame
void AObstacle::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

