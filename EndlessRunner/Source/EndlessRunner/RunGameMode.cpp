//// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunCharacter.h"
#include "Tile.h"
#include "Engine/World.h"

ARunGameMode::ARunGameMode()
{
	
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	for (int x = 0; x < 10; x++) {
		AddFloorTile();
	}
}

// https://youtu.be/9yftOwWp48A
void ARunGameMode::AddFloorTile() {
	FActorSpawnParameters SpawnParameters;
	FVector TileOffset = FVector(500, 0, 0);
	
	if (TileReference != nullptr) {
		ATile* NewTile = GetWorld()->SpawnActor<ATile>(TileReference, SpawnPoint, FRotator(0), SpawnParameters);
		SpawnPoint = FVector(NewTile->GetAttachPointLocation() + TileOffset);
		NewTile->OnTileExited.AddDynamic(this, &ARunGameMode::OnTileExit);
	}
}

void ARunGameMode::OnTileExit(ATile* Tile)
{
	AddFloorTile();
	SetDestroyTile(Tile);
}

//https://www.tomlooman.com/using-timers-in-ue4/

void ARunGameMode::SetDestroyTile(ATile* tile)
{
	FTimerHandle TimerHandler;
	FTimerDelegate TimerDel;

	TimerDel.BindLambda([tile] {
		tile->DestroyTile();
		});
	GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, 2.0f, false);
}


