// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "GameFramework/CharacterMovementComponent.h"



void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<ARunCharacter>(this->GetCharacter());
}

ARunCharacterController::ARunCharacterController() {
	PrimaryActorTick.bCanEverTick = true;
}

void ARunCharacterController::MoveRight(float scale) {
	Player->AddMovementInput(Player->GetActorRightVector(), scale);
}

void ARunCharacterController::MoveForward(float scale) {
	Player->AddMovementInput(Player->GetActorForwardVector(), scale);
}

void ARunCharacterController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	if (Player->GetisAlive() == true) {
		MoveForward(1);
	}
	
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}